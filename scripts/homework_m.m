clc
clear all

%loading dataset
data = readtable('lexdec.csv');

%load('lexdec');

%% Dataset

%The data we will be using is lexdec in the LanguageR package. 
%For discription of the data see Readme.txt. The variables we will be 
%focusing on in this homework and in class will be RT, Subject, and Frequency. 
%The question we plan to address is: what is the relationship of the frequency 
%of the word (Frequency) affect the subject's reaction time (RT).



%% Simple linear regression model

%The simplest way is to construct a simple linear regression model.

simple_lm = fitlm(data, 'RT~Frequency');

%% Varying intercepts
%Perhaps not every subjects has the same baseline reaction time. 
%Such a difference would be reflected as different subjects have varying 
%intercepts in the linear model. The model below accounts for such difference.

lme_1 = fitlme(data,'RT~Frequency+(1|Subject)');


%% Varying slope and intercept
%A more complex model would be that slopes and intercepts both change based on subjects

lme_2 = fitlme(data, 'RT~1+Frequency+(1+Frequency|Subject)');


%% Questions
% 1. What are the assumptions behind the mixed/multilevel model?


% 2. What are estimates, standard error, and R-Square in the three models? 
%    Why are these numbers different/not different?


% 3. Run the following 2 models. Compare them with the multilevel models.
%    What's missing in the code?
%    What do you observe in the output?

lm2 = fitlm(data, 'RT ~ 1 + Frequency + Subject');
lm3 = fitlm(data, 'RT ~ 1 + Frequency + Subject + Frequency*Subject');

