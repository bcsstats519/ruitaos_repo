Lexical decision latencies for 79 English nouns
Description
Lexical decision latencies elicited from 21 subjects for 79 English concrete nouns, with variables linked to subject or word.

Usage
data(lexdec)
Format
A data frame with 1659 observations on the following 28 variables.

Subject
a factor for the subjects.

RT
a numeric vector for logarithmically transformed reaction times.

Trial
a numeric vector for the rank of the trial in the experimental list.

Sex
a factor with levels F (female) and M (male).

NativeLanguage
a factor with levels English and Other, distinguishing between native and nonnative speakers of English.

Correct
a factor with levels correct and incorrect coding whether the word was correctly responded to as a word rather than a nonword.

PrevType
a factor with levels nonword and word coding whether the item presented at the preceding trial was a word or a nonword.

PrevCorrect
a factor with levels correct and incorrect coding whether the preceding item elicited a correct response.

Word
a factor with 79 words as levels.

Frequency
a numeric vector with logarithmically transformed lemma frequencies as available in the CELEX lexical database.

FamilySize
a numeric vector with the log-transformed count of a word's morphological family members.

SynsetCount
a numeric vector with the log-transformed count of synonym sets in WordNet in which the word is listed.

Length
a numeric vector for the word's length in letters.

Class
a factor for the semantic category of the word's referent, with levels animal and plant.

FreqSingular
a numeric vector with the frequency in CELEX of the singular form.

FreqPlural
a numeric vector with the frequency in CELEX of the plural form.

DerivEntropy
Shannon's entropy calculated over the frequency distribution of a word's family members.

Complex
a factor coding morphological complexity with levels complex and simplex.

rInfl
a numeric vector for the log of the ratio of the singular to the plural frequency.

meanRT
a numeric vector for the by-item mean reaction time averaged over subjects.

SubjFreq
a numeric vector for the by-item mean subjective frequency estimate averaged over subjects.

meanSize
a numeric vector for the by-item mean size rating averaged over subjects.

meanWeight
a numeric vector for the by-item mean weight rating averaged over subjects.

BNCw
a numeric vector with the logarithmically transformed frequency in the written part of the British National Corpus.

BNCc
a numeric vector with the logarithmically transformed frequency in the context-governed part of the British National Corpus.

BNCd
a numeric vector with the logarithmically transformed frequency in the demographic part of the British National Corpus.

BNCcRatio
a numeric vector with the log of the ratio of the (absolute) frequencies in the context-governed and written parts of the British National Corpus, normalized for the differences in corpus size.

BNCdRatio
a numeric vector with the log of the ratio of the (absolute) frequencies in the demographic and written parts of the British National Corpus, normalized for the differences in corpus size.

Source
Data collected with Jen Hay, University of Canterbury, Christchurch, New Zealand, 2004.

Examples
## Not run: 
data(lexdec)
require(lme4)
require(lmerTest)
require(optimx)

lexdec.lmer = lmer(RT ~ 1 + Correct + Trial + PrevType * meanWeight + 
  Frequency + NativeLanguage * Length + (1|Subject) + (1|Word), 
  control=lmerControl(optimizer="optimx",optCtrl=list(method="nlminb")),
  data = lexdec)
summary(lexdec.lmer)

# random slopes

lexdec.lmerA = lmer(RT ~ 1 + Correct + Trial + PrevType * meanWeight + 
  Frequency + NativeLanguage * Length + (Trial|Subject) + (1|Word), 
  control=lmerControl(optimizer="optimx",optCtrl=list(method="nlminb")),
  data = lexdec)
anova(lexdec.lmer, lexdec.lmerA)

lexdec.lmerB = lmer(RT ~ 1 + Correct + Trial + PrevType * meanWeight + 
  Frequency + NativeLanguage * Length + (Trial|Subject) + 
  (Length|Subject) + (1|Word), data = lexdec,
  control=lmerControl(optimizer="optimx",optCtrl=list(method="nlminb")))
anova(lexdec.lmerA, lexdec.lmerB)

# model criticism

qqnorm(resid(lexdec.lmerB))

lexdec.lmerC = lmer(RT ~ 1 + Correct + Trial + PrevType * meanWeight + 
  Frequency + NativeLanguage * Length + 
  (Trial|Subject) + (Length|Subject) + (1|Word), 
  data = lexdec[abs(scale(resid(lexdec.lmerB)))<2,],
  control=lmerControl(optimizer="optimx",optCtrl=list(method="nlminb")))

qqnorm(resid(lexdec.lmerC))

# p values
summary(lexdec.lmerC)


## End(Not run)